# Electronic Teachers Classroom Attendance Book Web

Demo [https://e-attendance-book.000webhostapp.com](https://e-attendance-book.000webhostapp.com/)

## Features

- Academic can login

- Academic can logout

- Academic can add staff

- Academic can add subject

- Academic can add class

- Academic can create timetable

## How to install

**Prerequisties**

- PHP >= 5.6.4

- MariaDB database

**Steps**

Step 1: Clone the repo

	`git clone url`

Step 2: Enter into the project directory

	`cd project-directory`

Step 3: Install php dependencies

	`composer install`

Step 4: Rename the `.env.example` to `.env` and edit `.env` to match your settings

Step 5: Navigate to database/migrations directory and import all the sql files to your database

Step 6: You are good to go
