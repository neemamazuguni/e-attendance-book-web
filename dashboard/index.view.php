<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                    <span class="glyphicon glyphicon-user icon-summary"></span>
                    <h4><?= count($users) ?></h4>
                    <span class="text-muted">Staff</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <span class="glyphicon glyphicon-book icon-summary"></span>
                    <h4><?= count($subjects) ?></h4>
                    <span class="text-muted">Subjects</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <span class="glyphicon glyphicon-tower icon-summary"></span>
                    <h4><?= count($classes) ?></h4>
                    <span class="text-muted">Classes</span>
                </div>
            </div>
            
        </div>
    </div>
</div>