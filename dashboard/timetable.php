<?php

include '../middleware/auth.php';

require_once '../config/database.php';

$sql = 'SELECT timetables.id as id, day, timetables.starts_at, timetables.ends_at, timetables.created_at, timetables.updated_at, subjects.name as subject, users.username as staff, users.first_name as first_name, users.last_name as last_name, classes.name as class FROM timetables, subjects, users, classes WHERE timetables.subject_id=subjects.id AND timetables.user_id=users.id AND timetables.class_id=classes.id;';

$stmt = $conn->prepare($sql);
$stmt->execute();

$timetables = $stmt->fetchAll(PDO::FETCH_ASSOC);

include '../templates/header.view.php';
include 'timetable.view.php';
include '../templates/footer.view.php';