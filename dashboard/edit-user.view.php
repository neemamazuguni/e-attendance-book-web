<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <h2 class="sub-header">Edit <?= $user->username ?></h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/subjects.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-user"></span> All Staff</a>
                </div>
                <div class="panel-body">
                    <form action="" method="POST" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="first_name" class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="first_name" id="name" class="form-control" value="<?= $user->first_name ?>"
                                       placeholder="First Name" title="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="last_name" id="name" class="form-control" value="<?= $user->last_name ?>"
                                       placeholder="Last Name" title="First Name">
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="username" id="name" class="form-control"
                                       value="<?= $user->username ?>"
                                       required="required" placeholder="Username" title="Username">
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-info-sign"></span> Username is a unique name that
                                    will be used to login.
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" id="password" class="form-control" value=""
                                       required="required" placeholder="Password" title="Password">
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-info-sign"></span> At least 8 characters, mix of
                                    lower and upper case, at least one numeric character and one alphanumeric character.
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Bio (Option)</label>
                            <div class="col-sm-10">
                                <textarea name="bio" id="bio" class="form-control" rows="5"
                                          placeholder="Bio"><?= $user->bio ?></textarea>
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-info-sign"></span> Bio
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>