<?php

include '../middleware/auth.php';

require_once '../config/database.php';

$stmt = $conn->prepare('SELECT * FROM classes;');
$stmt->execute();

$classes = $stmt->fetchAll(PDO::FETCH_ASSOC);

include '../templates/header.view.php';
include 'classes.view.php';
include '../templates/footer.view.php';