<?php

// check if user is logged in
include '../middleware/auth.php';

// pull the database connection
require_once '../config/database.php';

$stmt = $conn->prepare('SELECT users.id as user_id, users.first_name, users.last_name, users.username as username, roles.display_name as display_name, users.created_at as created_at, users.updated_at as updated_at FROM users INNER JOIN roles ON users.role_id=roles.id ORDER BY users.created_at asc;');
$stmt->execute();

// get all users and assign them to variable users
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

// var_dump($users); die();

include '../templates/header.view.php';
include 'users.view.php';
include '../templates/footer.view.php';
