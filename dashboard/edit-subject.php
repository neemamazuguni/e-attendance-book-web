<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['id'])) {
    if (!isset($_POST['name'])) {
        $errors[] = 'Subject is required.';
    }

    $sql = "UPDATE subjects SET name=:name, description=:description, updated_at=NOW() where id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':description', $_POST['description']);
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();

    header('Location: /dashboard/subjects.php');
}

if (isset($_GET['id'])) {
    $sql = "SELECT * FROM subjects WHERE id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();

    if ($stmt->rowCount() != 1) {
        echo 'No subject exist with that id';
        die();
    }
    $subject = $stmt->fetchObject();
}

include 'edit-subject.view.php';
include '../templates/footer.view.php';