<?php

require_once '../config/database.php';

if(isset($_GET['id'])) {
	$id = $_GET['id'];

	$sql = "DELETE FROM subjects WHERE id=:id";
	$stmt = $conn->prepare($sql);

	$stmt->bindParam(':id', $id);

	$stmt->execute();

	header('Location: ../dashboard/subjects.php');

}
