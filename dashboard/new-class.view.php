<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <h2 class="sub-header">New Class</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/subjects.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-th-list"></span> All</a>
                </div>
                <div class="panel-body">
                    <form action="" method="POST" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" id="name" class="form-control" value=""
                                       required="required" placeholder="Class name" title="Name">
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-info-sign"></span> Class name can be Form I, Form
                                    II etc.
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Description (Option)</label>
                            <div class="col-sm-10">
                                <textarea name="description" id="description" class="form-control" rows="5"
                                          placeholder="About the class"></textarea>
                                <p class="help-block">
                                    <span class="glyphicon glyphicon-info-sign"></span> Description of the class
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>