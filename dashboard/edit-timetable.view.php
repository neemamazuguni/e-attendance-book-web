<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>

            <h2 class="sub-header">Edit Timetable</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/subjects.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-th-list"></span> All</a>
                </div>
                <div class="panel-body">
                    <form action="" method="POST" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="day" class="col-sm-2 control-label">Day</label>
                            <div class="col-sm-10">
                                <select name="day" id="day" class="form-control">
                                    <option value="">-Select-</option>
                                    <option value="Monday"
                                    <?php if($timetable->day == 'Monday'): ?>
                                        selected
                                    <?php endif ?>
                                    >Monday</option>
                                    <option value="Tuesday"
                                    <?php if($timetable->day == 'Tuesday'): ?>
                                        selected
                                    <?php endif ?>
                                    >Tuesday</option>
                                    <option value="Wednesday"
                                    <?php if($timetable->day == 'Wednesday'): ?>
                                        selected
                                    <?php endif ?>
                                    >Wednesday</option>
                                    <option value="Thursday"
                                    <?php if($timetable->day == 'Thursday'): ?>
                                        selected
                                    <?php endif ?>
                                    >Thursday</option>
                                    <option value="Friday"
                                    <?php if($timetable->day == 'Friday'): ?>
                                        selected
                                    <?php endif ?>
                                    >Friday</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Subject</label>
                            <div class="col-sm-10">
                                <select name="subject" id="subject" class="form-control">
                                    <option value="">-Select-</option>
                                    <?php foreach ($subjects as $subject): ?>
                                        <option value="<?= $subject['id'] ?>"
                                        <?php if($timetable->subject == $subject['name']): ?>
                                        selected
                                    <?php endif ?>
                                        ><?= $subject['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="staff" class="col-sm-2 control-label">Staff</label>
                            <div class="col-sm-10">
                                <select name="staff" id="staff" class="form-control">
                                    <option value="">-Select-</option>
                                    <?php foreach ($users as $user): ?>
                                        <option value="<?= $user['id'] ?>"

                                        <?php if($timetable->user_id == $user['id']): ?>
                                        selected
                                        <?php endif ?>
                                        ><?= $user['first_name'] ?>
                                        <?= $user['last_name'] ?></option> 
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="class" class="col-sm-2 control-label">Class</label>
                            <div class="col-sm-10">
                                <select name="class" id="class" class="form-control">
                                    <option value="">-Select-</option>
                                    <?php foreach ($classes as $class): ?>
                                        <option value="<?= $class['id'] ?>"
                                        <?php if($timetable->class == $class['name']): ?>
                                        selected
                                    <?php endif ?>
                                        ><?= $class['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="starts_at" class="col-sm-2 control-label">Starts at</label>
                            <div class="col-sm-10">
                                <input type="time" name="starts_at" id="starts_at" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ends_at" class="col-sm-2 control-label">Ends at</label>
                            <div class="col-sm-10">
                                <input type="time" name="ends_at" id="ends_at" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>