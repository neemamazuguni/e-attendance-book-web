<?php
include '../templates/nav.view.php';
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>


            <h2 class="sub-header">Attendance</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <?php if (count($attendances) > 0): ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Day</th>
                                    <th>Subject</th>
                                    <th>Staff</th>
                                    <th>Class</th>
                                    <th>Starts at</th>
                                    <th>Comments</th>
                                    <th>Ends at</th>
                                    <th>Attended On</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($attendances as $attendance): ?>
                                    <tr>
                                        <td><?= $attendance['id'] ?>.</td>
                                        <td><?= $attendance['day'] ?></td>
                                        <td><?= $attendance['subject'] ?></td>
                                        <td>
                                            <?= $attendance['first_name'] ?> <?= $attendance['last_name'] ?>
                                        </td>
                                        <td><?= $attendance['class'] ?></td>
                                        <td><?= $attendance['starts_at'] ?></td>
                                        <td><?= $attendance['comment'] ?></td>
                                        <td><?= $attendance['ends_at'] ?></td>
                                        <td><?= $attendance['created_at'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            No data
                        </div>
                    <?php endif ?>
                </div>
                <div class="panel-footer">
                    Total = <?= count($attendances) ?>
                </div>
            </div>
        </div>
    </div>
</div>