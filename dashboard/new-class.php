<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['name'])) {
        $errors[] = 'Class name is required.';
    }

    $sql = "INSERT INTO classes (name, description) VALUES (:name, :description)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':description', $_POST['description']);

    $stmt->execute();

    header('Location: /dashboard/classes.php');
}

include 'new-class.view.php';
include '../templates/footer.view.php';