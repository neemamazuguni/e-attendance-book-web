<?php
include '../templates/nav.view.php';
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>


            <h2 class="sub-header">Classes</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/new-class.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-link"></span> New</a>
                </div>
                <div class="panel-body">
                    <?php if (count($classes) > 0): ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($classes as $class): ?>
                                    <tr>
                                        <td><?= $class['id'] ?>.</td>
                                        <td><?= $class['name'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            No classes
                        </div>
                    <?php endif ?>
                </div>
                <div class="panel-footer">
                    Total = <?= count($classes) ?>
                </div>
            </div>
        </div>
    </div>
</div>