<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['day'])) {
        $errors[] = 'Day is required.';
    }
    if (!isset($_POST['subject'])) {
        $errors[] = 'Subject is required.';
    }
    if (!isset($_POST['staff'])) {
        $errors[] = 'Staff is required.';
    }
    if (!isset($_POST['class'])) {
        $errors[] = 'Class is required.';
    }
    if (!isset($_POST['starts_at'])) {
        $errors[] = 'Start time is required.';
    }
    if (!isset($_POST['ends_at'])) {
        $errors[] = 'Ending time is required.';
    }


    $sql = "INSERT INTO timetables (day, subject_id, user_id, class_id, starts_at, ends_at) VALUES (:day, :subject, :staff, :class, :starts_at, :ends_at)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':day', $_POST['day']);
    $stmt->bindParam(':subject', $_POST['subject']);
    $stmt->bindParam(':staff', $_POST['staff']);
    $stmt->bindParam(':class', $_POST['class']);
    $stmt->bindParam(':starts_at', date("Y-m-d H:i:s", strtotime($_POST['starts_at'])));
    $stmt->bindParam(':ends_at', date("Y-m-d H:i:s", strtotime($_POST['ends_at'])));

    $stmt->execute();

    header('Location: /dashboard/timetable.php');
}

// Subjects
$stmt = $conn->prepare('SELECT * FROM subjects;');
$stmt->execute();
$subjects = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Users
$stmt = $conn->prepare('SELECT * FROM users;');
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Classes
$stmt = $conn->prepare('SELECT * FROM classes;');
$stmt->execute();
$classes = $stmt->fetchAll(PDO::FETCH_ASSOC);

include 'new-timetable.view.php';
include '../templates/footer.view.php';