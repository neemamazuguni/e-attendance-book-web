<?php
include '../templates/nav.view.php';
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>


            <h2 class="sub-header">This Week Summary</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <div class="panel-body">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>DAY</th>
                                <th>PERIODS ATTENDED</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($attendances as $attendance): ?>
                            <tr>
                                <td><?=$attendance['day']?></td>
                                <td><?=$attendance['attendance_count']?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>
                <div class="panel-footer">
                </div>
            </div>
        </div>
    </div>
</div>