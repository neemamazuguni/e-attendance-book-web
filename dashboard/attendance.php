<?php

include '../middleware/auth.php';

require_once '../config/database.php';

$sql = 'SELECT timetables.id as id, day, timetables.starts_at, timetables.ends_at, attendance.created_at, attendance.updated_at, subjects.name as subject, users.username as staff, users.first_name as first_name, users.last_name as last_name, classes.name as class, attendance.comment as comment FROM timetables, subjects, users, classes, attendance WHERE timetables.subject_id=subjects.id AND timetables.user_id=users.id AND timetables.class_id=classes.id AND attendance.timetable_id=timetables.id;';

$stmt = $conn->prepare($sql);
$stmt->execute();

$attendances = $stmt->fetchAll(PDO::FETCH_ASSOC);

// var_dump($attendances);die();

include '../templates/header.view.php';
include 'attendance.view.php';
include '../templates/footer.view.php';