<?php

include '../middleware/auth.php';

require_once '../config/database.php';

// users
$stmt = $conn->prepare('SELECT * FROM users;');
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

// subjects
$stmt = $conn->prepare('SELECT * FROM subjects;');
$stmt->execute();
$subjects = $stmt->fetchAll(PDO::FETCH_ASSOC);

// classes
$stmt = $conn->prepare('SELECT * FROM classes;');
$stmt->execute();
$classes = $stmt->fetchAll(PDO::FETCH_ASSOC);



include '../templates/header.view.php';
include 'index.view.php';
include '../templates/footer.view.php';