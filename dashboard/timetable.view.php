<?php
include '../templates/nav.view.php';
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>


            <h2 class="sub-header">Timetable</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/new-timetable.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-link"></span> New</a>
                </div>
                <div class="panel-body">
                    <?php if (count($timetables) > 0): ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Day</th>
                                    <th>Subject</th>
                                    <th>Staff</th>
                                    <th>Class</th>
                                    <th>Starts at</th>
                                    <th>Ends at</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($timetables as $timetable): ?>
                                    <tr>
                                        <td><?= $timetable['id'] ?>.</td>
                                        <td><?= $timetable['day'] ?></td>
                                        <td><?= $timetable['subject'] ?></td>
                                        <td>
                                            <?= $timetable['first_name'] ?> 
                                            <?= $timetable['last_name'] ?> 
                                        </td>
                                        <td><?= $timetable['class'] ?></td>
                                        <td><?= $timetable['starts_at'] ?></td>
                                        <td><?= $timetable['ends_at'] ?></td>
                                        <td><?= $timetable['created_at'] ?></td>
                                        <td><?= $timetable['updated_at'] ?></td>
                                        <td>
                                            <a href="/dashboard/edit-timetable.php?id=<?= $timetable['id'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        </td>
                                        <td>
                                            

                                            <!-- Button trigger modal -->
                                            <button type="button" class="text-danger" data-toggle="modal" data-target="#myModal<?= $timetable['id'] ?>">
                                              <span class="glyphicon glyphicon-trash"></span>
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?= $timetable['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    Are you sure that you want to delete record?
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <a href="/dashboard/delete-timetable.php?id=<?= $timetable['id'] ?>" class="btn btn-primary">Yes</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>


                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            No data
                        </div>
                    <?php endif ?>
                </div>
                <div class="panel-footer">
                    Total = <?= count($timetables) ?>
                </div>
            </div>
        </div>
    </div>
</div>