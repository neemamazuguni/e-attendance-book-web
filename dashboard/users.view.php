<?php
include '../templates/nav.view.php';
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <?php
            include '../templates/sidebar.view.php';
            ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Dashboard</h1>


            <h2 class="sub-header">Staff</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/dashboard/new-user.php" class="btn btn-primary"><span
                                class="glyphicon glyphicon-link"></span> New</a>
                </div>
                <div class="panel-body">
                    <?php if (count($users) > 0): ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?= $user['user_id'] ?>.</td>
                                        <td><?= $user['first_name'] ?>.</td>
                                        <td><?= $user['last_name'] ?>.</td>
                                        <td><?= $user['username'] ?></td>
                                        <td>
                                            <?php if($user['display_name'] == 'Student'): ?>
                                                <span class="label label-primary">
                                                    <?= $user['display_name'] ?>
                                                </span>
                                            <?php endif ?>
                                            <?php if($user['display_name'] == 'Teacher'): ?>
                                                <span class="label label-success">
                                                    <?= $user['display_name'] ?>
                                                </span>
                                            <?php endif ?>
                                            <?php if($user['display_name'] == 'Academic'): ?>
                                                <span class="label label-danger">
                                                    <?= $user['display_name'] ?>
                                                </span>
                                            <?php endif ?>                                                                                        
                                        </td>
                                        <td><?= $user['created_at'] ?></td>
                                        <td><?= $user['updated_at'] ?></td>
                                        <td>
                                            <a href="/dashboard/edit-user.php?id=<?= $user['user_id'] ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        </td>
                                        <td>

                                            
                                                          <!-- Button trigger modal -->
                                            <button type="button" class="text-danger" data-toggle="modal" data-target="#myModal<?= $user['user_id'] ?>">
                                              <span class="glyphicon glyphicon-trash"></span>
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?= $user['user_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    Are you sure that you want to delete this user?
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <a href="/dashboard/delete-user.php?id=<?= $user['user_id'] ?>" class="btn btn-primary">Yes</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>



                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-info">
                            No data
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>