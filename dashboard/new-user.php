<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['username'])) {
        $errors[] = 'Username is required.';
    }

    $sql = "INSERT INTO users (first_name, last_name, username, password, bio, role_id, created_at, updated_at) VALUES (:first_name, :last_name, :username, :password, :bio, :role_id, NOW(), NOW())";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':first_name', $_POST['first_name']);
    $stmt->bindParam(':last_name', $_POST['last_name']);
    $stmt->bindParam(':username', $_POST['username']);
    $stmt->bindParam(':password', $_POST['password']);
    $stmt->bindParam(':bio', $_POST['bio']);
    $stmt->bindParam(':role_id', $_POST['role_id']);

    $stmt->execute();

    header('Location: /dashboard/users.php');
}

// Fetch roles
$sql = "SELECT * FROM roles";
$stmt = $conn->prepare($sql);

$stmt->execute();

$roles = $stmt->fetchAll(PDO::FETCH_ASSOC);

include 'new-user.view.php';
include '../templates/footer.view.php';