<?php

include '../middleware/auth.php';

require_once '../config/database.php';

$stmt = $conn->prepare('SELECT * FROM subjects;');
$stmt->execute();

$subjects = $stmt->fetchAll(PDO::FETCH_ASSOC);

include '../templates/header.view.php';
include 'subjects.view.php';
include '../templates/footer.view.php';