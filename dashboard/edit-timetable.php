<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_GET['id']) {
    if (!isset($_POST['day'])) {
        $errors[] = 'Day is required.';
    }
    if (!isset($_POST['subject'])) {
        $errors[] = 'Subject is required.';
    }
    if (!isset($_POST['staff'])) {
        $errors[] = 'Staff is required.';
    }
    if (!isset($_POST['class'])) {
        $errors[] = 'Class is required.';
    }
    if (!isset($_POST['starts_at'])) {
        $errors[] = 'Start time is required.';
    }
    if (!isset($_POST['ends_at'])) {
        $errors[] = 'Ending time is required.';
    }


    $sql = "UPDATE timetables SET day=:day, subject_id=:subject, user_id=:staff, class_id=:class, starts_at=:starts_at, ends_at=:ends_at WHERE id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':day', $_POST['day']);
    $stmt->bindParam(':subject', $_POST['subject']);
    $stmt->bindParam(':staff', $_POST['staff']);
    $stmt->bindParam(':class', $_POST['class']);
    $stmt->bindParam(':starts_at', date("Y-m-d H:i:s", strtotime($_POST['starts_at'])));
    $stmt->bindParam(':ends_at', date("Y-m-d H:i:s", strtotime($_POST['ends_at'])));
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();

    header('Location: /dashboard/timetable.php');
}

if (isset($_GET['id'])) {

    $id = $_GET['id'];
    
    $sql = 'SELECT timetables.id as id, day, timetables.starts_at, timetables.ends_at, timetables.created_at, timetables.updated_at, subjects.name as subject, users.username as staff, users.id as user_id, users.first_name as first_name, users.last_name as last_name, classes.name as class FROM timetables, subjects, users, classes WHERE timetables.subject_id=subjects.id AND timetables.user_id=users.id AND timetables.class_id=classes.id AND timetables.id=:id;';

    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id', $id);


    $stmt->execute();

    $timetable = $stmt->fetchObject();

    // var_dump($timetable);die();

}

// Subjects
$stmt = $conn->prepare('SELECT * FROM subjects;');
$stmt->execute();
$subjects = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Users
$stmt = $conn->prepare('SELECT * FROM users;');
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Classes
$stmt = $conn->prepare('SELECT * FROM classes;');
$stmt->execute();
$classes = $stmt->fetchAll(PDO::FETCH_ASSOC);

include 'edit-timetable.view.php';
include '../templates/footer.view.php';