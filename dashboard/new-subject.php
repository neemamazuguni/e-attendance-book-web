<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!isset($_POST['name'])) {
        $errors[] = 'Subject is required.';
    }

    $sql = "INSERT INTO subjects (name, description) VALUES (:name, :description)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':name', $_POST['name']);
    $stmt->bindParam(':description', $_POST['description']);

    $stmt->execute();

    header('Location: /dashboard/subjects.php');
}

include 'new-subject.view.php';
include '../templates/footer.view.php';