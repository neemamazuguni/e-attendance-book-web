<?php

include '../middleware/auth.php';

require_once '../config/database.php';

include '../templates/header.view.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['id'])) {
    if (!isset($_POST['username'])) {
        $errors[] = 'Username is required.';
    }

    $sql = "UPDATE users SET first_name=:first_name, last_name=:last_name, username=:username, password=:password, bio=:bio, updated_at=NOW() WHERE id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':first_name', $_POST['first_name']);
    $stmt->bindParam(':last_name', $_POST['last_name']);
    $stmt->bindParam(':username', $_POST['username']);
    $stmt->bindParam(':password', $_POST['password']);
    $stmt->bindParam(':bio', $_POST['bio']);
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();

    header('Location: /dashboard/users.php');
}

if (isset($_GET['id'])) {
    $sql = "SELECT * FROM users WHERE id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();

    if ($stmt->rowCount() != 1) {
        echo 'No user exist with that id';
        die();
    }
    $user = $stmt->fetchObject();
    // var_dump($user);die();
}

include 'edit-user.view.php';
include '../templates/footer.view.php';