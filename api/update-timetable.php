<?php

header('Access-Control-Allow-Origin:*');

require_once '../config/database.php';


if ($_SERVER['REQUEST_METHOD'] == "GET") {
    echo 'post request';

    if (isset($_GET['id']) && isset($_GET['attended']) && isset($_GET['verified'])) {
        $id = $_GET['id'];
        $attended = $_GET['attended'];
        $verified = $_GET['verified'];
        $comment = $_GET['comment'];

        $sql = "UPDATE timetables SET is_attended=:attended, is_verified=:verified, comment=:comment WHERE id=:id";

        $stmt = $conn->prepare($sql);

        $stmt->bindParam(':attended', $attended);
        $stmt->bindParam(':verified', $verified);
        $stmt->bindParam(':comment', $comment);
        $stmt->bindParam(':id', $id);

        $stmt->execute();

        // Create attendance
        $sql = "INSERT INTO attendance (timetable_id,comment) VALUES (:id, :comment)";
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':comment', $comment);

        $stmt->execute();

        echo json_encode(['updated' => true]);
    } else {
        echo json_encode(['updated' => false]);
    }
}
