<?php

if (isset($_GET['username']) && isset($_GET['password'])) {

    header('Access-Control-Allow-Origin:*');

    require_once '../config/database.php';

    $stmt = $conn->prepare('SELECT * FROM users WHERE username=? AND password=?');

    $username = $_GET['username'];
    $password = $_GET['password'];

    $stmt->bindParam(1, $username);
    $stmt->bindParam(2, $password);
    $stmt->execute();

    $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($stmt->rowCount() == 1) {
        echo "true";
    } else {
        echo "false";
    }
} else {
    echo 'error! no data supplied';
}

