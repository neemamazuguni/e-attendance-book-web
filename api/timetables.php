<?php

header('Access-Control-Allow-Origin:*');

require_once '../config/database.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {

    $sql = 'SELECT timetables.id as id, day, timetables.starts_at, timetables.ends_at, timetables.created_at, timetables.updated_at, subjects.name as subject, users.username as staff, classes.name as class FROM timetables, subjects, users, classes WHERE timetables.subject_id=subjects.id AND timetables.user_id=users.id AND timetables.class_id=classes.id AND timetables.is_attended=false AND timetables.is_verified=false';

//    if (isset($_GET['darasa']) && !isset($_GET['siku'])) {
//        $sql .= ' WHERE class_id=?';
//    }
//    if (isset($_GET['siku']) && !isset($_GET['darasa'])) {
//        $sql .= ' WHERE day=?';
//    }
//    if (isset($_GET['darasa']) && isset($_GET['siku'])) {
//        $sql .= ' WHERE class=? AND day=?';
//    }

    $stmt = $conn->prepare($sql);

    $stmt->bindParam(1, $_GET['darasa']);
    $stmt->bindParam(2, $_GET['siku']);
    $stmt->execute();

    $timetables = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($timetables);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    echo 'post request';

    if (isset($_POST['id']) && isset($_POST['attended']) && isset($_POST['verified'])) {
        $id = $_POST['id'];
        $attended = $_POST['attended'];
        $verified = $_POST['verified'];
        $comment = $_POST['comment'];

        $sql = "UPDATE timetables SET is_attended=:attended, is_verified=:verified, comment=:comment WHERE id=:id";

        $stmt = $conn->prepare($sql);

        $stmt->bindParam(':attended', $attended);
        $stmt->bindParam(':verified', $verified);
        $stmt->bindParam(':comment', $comment);
        $stmt->bindParam(':id', $id);

        $stmt->execute();

        echo json_encode(['updated' => true]);
    } else {
        echo json_encode(['updated' => false]);
    }
}
