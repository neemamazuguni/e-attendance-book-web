<?php

header('Access-Control-Allow-Origin:*');

require_once '../config/database.php';

$sql = 'SELECT timetables.id as id, count(*) as attendance_count, day, timetables.starts_at, timetables.ends_at, attendance.created_at, attendance.updated_at, subjects.name as subject, users.username as staff, classes.name as class, attendance.comment as comment FROM timetables, subjects, users, classes, attendance WHERE timetables.subject_id=subjects.id AND timetables.user_id=users.id AND timetables.class_id=classes.id AND attendance.timetable_id=timetables.id GROUP BY day;';

$stmt = $conn->prepare($sql);
$stmt->execute();

$attendances = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($attendances);
