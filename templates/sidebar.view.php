<ul class="nav nav-sidebar">
    <li class="active"><a href="/dashboard">Overview <span class="sr-only">(current)</span></a></li>
    <li><a href="/dashboard/summary.php">Summary</a></li>
<!--     <li><a href="#">Analytics</a></li>
    <li><a href="#">Export</a></li> -->
</ul>
<ul class="nav nav-sidebar">
    <li><a href="/dashboard/users.php"><span class="glyphicon glyphicon-user"></span> Staff</a></li>
    <li><a href="/dashboard/subjects.php"><span class="glyphicon glyphicon-book"></span> Subjects</a></li>
    <li><a href="/dashboard/classes.php"><span class="glyphicon glyphicon-star-empty"></span> Classes</a></li>
    <li><a href="/dashboard/timetable.php"><span class="glyphicon glyphicon-calendar"></span> Timetable</a></li>
    <li><a href="/dashboard/attendance.php"><span class="glyphicon glyphicon-check"></span> Attendance</a></li>
</ul>