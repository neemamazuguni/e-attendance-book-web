<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">E-Attendance Book</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_SESSION['username'])): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                                    class="glyphicon glyphicon-user"></span> <?= $_SESSION['username'] ?> <b
                                    class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/dashboard"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a>
                            </li>
                            <li><a href="#"><span class="glyphicon glyphicon-wrench"></span> Settings</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-picture"></span> Profile</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span> Help</a></li>
                            <li><a href="/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                        </ul>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</nav>