<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                &copy; <?php date('Y') ?> E-Attendance Book | <a href="#">Terms</a>
            </div>
        </div>
    </div>
</footer>