<?php

require_once '../config/database.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM users WHERE username=:username AND  password=:password;";

    $stmt = $conn->prepare($sql);

    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', $password);

    $stmt->execute();

    $user = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (count($user) == 1) {
        session_start();

        $_SESSION['username'] = $user[0]['username'];

        header('Location: /dashboard');
    } else {
        $errors[] = 'Invalid username or password';
    }
}

include 'login.view.php';
