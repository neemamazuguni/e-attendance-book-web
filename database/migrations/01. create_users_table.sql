CREATE TABLE users(
  id INT (11) PRIMARY KEY auto_increment,
  first_name VARCHAR (255) NULL ,
  last_name VARCHAR (255) NULL ,
  username VARCHAR (255) NOT  NULL UNIQUE ,
  password VARCHAR (255) NOT  NULL ,
  bio TEXT NULL,
  role_id INT(11),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users (username, password, role_id) VALUES ('academic', 'secret', 3);
