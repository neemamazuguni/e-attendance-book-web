CREATE TABLE attendance (
  id INT (11) PRIMARY KEY auto_increment,
  timetable_id INT(11),
  comment TEXT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
