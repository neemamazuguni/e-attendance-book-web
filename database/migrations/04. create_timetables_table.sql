CREATE TABLE timetables (
  id INT (11) PRIMARY KEY auto_increment,
  starts_at TIMESTAMP ,
  ends_at TIMESTAMP ,
  user_id VARCHAR(255) ,
  subject_id VARCHAR (255),
  class_id VARCHAR (255),
  day VARCHAR (255),
  is_attended BOOLEAN DEFAULT FALSE ,
  is_verified BOOLEAN DEFAULT  FALSE ,
  comment TEXT NULL ,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
