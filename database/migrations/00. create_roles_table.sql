CREATE TABLE roles(
  id INT (11) PRIMARY KEY auto_increment,
  name VARCHAR (255),
  display_name VARCHAR (255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO roles (name, display_name) VALUES ('student', 'Student');
INSERT INTO roles (name, display_name) VALUES ('teacher', 'Teacher');
INSERT INTO roles (name, display_name) VALUES ('academic', 'Academic');
